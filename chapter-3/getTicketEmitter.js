import { EventEmitter } from 'events'

export default (number, callback) => {
  const emitter = new EventEmitter()
  const ticks = 0
  setTimeout(() => {
    return callback(null, ticks)
  }, number)
  setImmediate(emitTick(ticks, emitter, callback))
  if (number > 50) {
    const numberOfTicks = Math.floor(number / 50)
    let finalCallback = emitTick(ticks, emitter, callback)

    for (let i = 0; i < numberOfTicks; i++) {
      finalCallback = includeNewCallback(finalCallback, emitTick(ticks, emitter, callback))
    }
    setTimeout(finalCallback, 50)
  }
  return emitter
}

const includeNewCallback = (prevCallback, newCallback) => {
  return () => {
    newCallback()
    setTimeout(prevCallback, 50)
  }
}

const checkDateIsDivisbleByFive = () => Date.now() % 5 === 0

const emitTick = (ticks, emitter, callback) => () => {
  if (checkDateIsDivisbleByFive()) {
    return callback(Error('Date divisible by 5'), null)
  }
  ticks++
  emitter.emit('tick', ticks)
}
