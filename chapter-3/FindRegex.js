import { EventEmitter } from 'events'
import { readFile } from 'fs'

class FindRegex extends EventEmitter {
  constructor (regex) {
    super()
    this.regex = regex
    this.files = []
  }

  addFile (file) {
    this.files.push(file)
    return this
  }

  find () {
    setImmediate(() => this.emit('startFind', this.files))
    for (const file of this.files) {
      readFile(file, 'utf8', (error, fileContent) => {
        if (error) {
          return this.emit('error', error)
        }
        this.emit('fileRead', file)

        const match = fileContent.match(this.regex)
        if (match) {
          match.forEach(matchElement => this.emit('found', file, matchElement))
        }
        if (file === this.files[this.files.length - 1]) {
          setImmediate(() => this.emit('endFind'))
        }
      })
    }
    return this
  }
}

export default FindRegex
