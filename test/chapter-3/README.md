# Chapter 3 exercises

## 3.1. A simple event

Modify the asynchronous `FindRegex` class so that it emits an event when the find process starts, passing
the input files list as an argument. Hint: beware of [Zalgo][1]!

## 3.2. Ticker

Write a function that accepts a `number` and a `callback` as the arguments. The function will return an `EventEmitter` 
that emits an event called `tick` every 50 miliseconds until the `number` of milliseconds is passed from the invocation
of the function. The function will also call the `callback` when the `number` of millisenconds has passed, providing, as
the result, the total count of tick events emitted. Hint: you can use `setTimeout()` to schedule another `setTimeout()`
recursively.

## 3.3. A simple modification

Modify the function created in the exerxise 3.2 so that it emits a `tick` event inmediately after the function is invoked.

## 3.4. Playing with errors

Modify the function created in the exercise `3.3` so that it produces an error if the timestamp at the moment of a `tick`
(including the inital one that we added as part of the exercise `3.3`) is divisible by `5`. Propagate the error using both 
the callback and the event emitter. Hint: use the `Date.now()` to get the timestamp and the reminder (`%`) operator
to check whether the timestamp is divisible by `5`.

[1]: https://nodejsdp.link/unleashing-zalgo