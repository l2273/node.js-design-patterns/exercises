import { expect } from 'chai'
import getTickerEmitter from '../../chapter-3/getTicketEmitter.js'
import stubDateNow, { resetDateNow } from './stubDateNow.js'

describe('Ticker exercise', () => {
  afterEach(() => {
    resetDateNow()
  })
  let ticks
  beforeEach(() => {
    ticks = 0
    stubDateNow(123)
  })

  const checkNumberOfTicks = (numberOfTicks) => {
    ticks++
    expect(numberOfTicks).to.be.equal(ticks)
  }

  it('Given number 20 milliseconds, when getTicker should return one tick only', (done) => {
    const tickerEmitter = getTickerEmitter(20, (error, ticks) => {
      if (error) {
        done(error)
      }
      expect(ticks).to.be.equal(1)
      tickerEmitter.removeListener('tick', checkNumberOfTicks)
      done()
    })

    tickerEmitter.on('tick', checkNumberOfTicks)
  })

  it('Given number 60 milliseconds, when getTicker should return two ticks only', (done) => {
    const tickerEmitter = getTickerEmitter(60, (error, ticks) => {
      if (error) {
        done(error)
      }
      expect(ticks).to.be.equal(2)
      tickerEmitter.removeListener('tick', checkNumberOfTicks)
      done()
    })

    tickerEmitter.on('tick', checkNumberOfTicks)
  })

  it('Given number 110 milliseconds, when getTicker should return three ticks', (done) => {
    const tickerEmitter = getTickerEmitter(110, (error, ticks) => {
      if (error) {
        done(error)
      }
      expect(ticks).to.be.equal(3)
      tickerEmitter.removeListener('tick', checkNumberOfTicks)
      done()
    })

    tickerEmitter.on('tick', checkNumberOfTicks)
  })

  it('Given number 420 milliseconds, when getTicker should return nine ticks', (done) => {
    const tickerEmitter = getTickerEmitter(420, (error, ticks) => {
      if (error) {
        done(error)
      }
      expect(ticks).to.be.equal(9)
      tickerEmitter.removeListener('tick', checkNumberOfTicks)
      tickerEmitter.removeAllListeners()
      done()
    })

    tickerEmitter.on('tick', checkNumberOfTicks)
  })
})
