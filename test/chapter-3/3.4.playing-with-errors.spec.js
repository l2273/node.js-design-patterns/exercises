import { expect } from 'chai'
import getTicketEmitter from '../../chapter-3/getTicketEmitter.js'
import stubDateNow from './stubDateNow.js'

describe('Playing with errors', () => {
  afterEach(() => {
    const realDateNow = Date.now.bind(global.Date)
    global.Date.now = realDateNow
  })
  it('Checking if the date is mocked', () => {
    stubDateNow(123)

    expect(Date.now()).to.be.equal(123)
  })
  it('When timestamp is divisible by 5, an error is propagated', (done) => {
    stubDateNow(125)

    getTicketEmitter(120, (error, ticks) => {
      expect(error).to.be.a('error')
      done()
    })
  })
})
