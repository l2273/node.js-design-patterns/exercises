import { expect } from 'chai'
import getTickerEmitter from '../../chapter-3/getTicketEmitter.js'
import stubDateNow, { resetDateNow } from './stubDateNow.js'

describe('getTicketEmitter simple modification', () => {
  afterEach(() => {
    resetDateNow()
  })
  let ticks
  beforeEach(() => {
    ticks = 0
    stubDateNow(123)
  })

  const checkNumberOfTicks = (numberOfTicks) => {
    ticks++
    expect(numberOfTicks).to.be.equal(ticks)
  }

  it('Has to emit a tick event just after it has been called', (done) => {
    const tickerEmitter = getTickerEmitter(60, (error, ticks) => {
      console.log(error, ticks)
      if (error) {
        done(error)
      }
      expect(ticks).to.be.equal(2)
      tickerEmitter.removeListener('tick', checkNumberOfTicks)
      done()
    })

    tickerEmitter.on('tick', checkNumberOfTicks)
  })
})
