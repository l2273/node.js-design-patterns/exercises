import { expect } from 'chai'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'
import FindRegex from './../../chapter-3/FindRegex.js'

const __fileName = fileURLToPath(import.meta.url)
const __dirname = dirname(__fileName)

describe('A simple event', () => {
  let findRegex
  const pathFile = path.join(__dirname, 'resources', 'example.txt')
  const pathFile2 = path.join(__dirname, 'resources', 'example1.txt')
  beforeEach(() => {
    findRegex = new FindRegex(/hello w\w+/g)
  })
  it('Using FidRegex', () => {
    expect(1).to.be.equal(1)
  })
  it('When add file, files should increase', () => {
    findRegex.addFile('./example.txt')

    expect(findRegex.files.length).to.be.equal(1)
  })
  it('When find should emit one found', (done) => {
    const testParametrization = {
      expectedFound: 3
    }
    const testResult = {
      founds: 0
    }

    findRegex.addFile(pathFile)
      .find()
      .on('found', () => {
        testResult.founds++
      })
      .on('error', (error) => done(error))
      .on('endFind', () => {
        expect(testResult.founds).to.be.equal(testParametrization.expectedFound)
        done()
      })
  })
  it('When find should emit one fileRead', (done) => {
    const testParametrization = {
      expectedFileRead: 1
    }
    const testResult = {
      fileReads: 0
    }

    findRegex.addFile(pathFile)
      .find()
      .on('fileRead', (file) => {
        testResult.fileReads++
      })
      .on('error', (error) => done(error))
      .on('endFind', () => {
        expect(testResult.fileReads).to.be.equal(testParametrization.expectedFileRead)
        done()
      })
  })
  it('Given two files, When find should emit two fileRead', (done) => {
    const testParametrization = {
      expectedFileRead: 2
    }
    const testResult = {
      fileReads: 0
    }

    findRegex
      .addFile(pathFile)
      .addFile(pathFile2)
      .find()
      .on('fileRead', (file) => {
        testResult.fileReads++
      })
      .on('error', (error) => done(error))
      .on('endFind', () => {
        expect(testResult.fileReads).to.be.equal(testParametrization.expectedFileRead)
        done()
      })
  })

  it('Given wrong file should emit error', (done) => {
    findRegex.addFile('notExestingFile.txt')
      .find()
      .on('error', () => done())
  })

  it('Given two files, emits findStarts if listener before', (done) => {
    findRegex
      .addFile(pathFile2)
      .addFile(pathFile)
      .on('startFind', () => done())
      .find()
  })
  it('Given two files, emits findStarts if listener after', (done) => {
    findRegex
      .addFile(pathFile2)
      .addFile(pathFile)
      .find()
      .on('startFind', () => done())
  })
  it('Given two files, emits findStarts with list of files', (done) => {
    findRegex
      .addFile(pathFile2)
      .addFile(pathFile)
      .find()
      .on('startFind', (files) => {
        expect(files.length).to.be.equal(2)
        done()
      })
  })
})
