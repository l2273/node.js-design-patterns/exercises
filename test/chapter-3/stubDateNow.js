export default (milliseconds) => {
  global.Date.now = () => milliseconds
}

export const resetDateNow = () => {
  const realDateNow = Date.now.bind(global.Date)
  global.Date.now = realDateNow
}
